﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlackJack
{

    public partial class Main : System.Web.UI.Page
    {
        public static Random rnd = new Random();
        Image card1 = new Image();// instantiating the control
        Image card2 = new Image(); // instantiating the control
        Image players_card = new Image(); // instantiating the control     
        
        

        public static string[,] deck = new string[,]{{"2C","2D","2H","2S"},{"3C","3D","3H","3S"},{"4C","4D","4H","4S"},{"5C","5D","5H","5S"},
            {"6C","6D","6H","6S"},{"7C","7D","7H","7S"},{"8C","8D","8H","8S"},{"9C","9D","9H","9S"},{"10C","10D","10H","10S"},
        {"JC","JD","JH","JS"},{"QC","QD","QH","QS"},{"KC","KD","KH","KS"},{"AC","AD","AH","AS"}};


      
        public void endMatch(double wallet)
        {
            if(wallet <= 0)
            {
                whoWon.Visible = true;
                gameOver.Text = "GAME OVER"+Environment.NewLine+ "Insufficient Funds.";
                @continue.Visible = false;

            }
        }
        public string generateCard(Player p)
        {
           
            int i = rnd.Next(0, 13); // creates a number between 0 and 12
            int j = rnd.Next(0, 4);   // creates a number between 0 and 3

            while (deck[i, j].Equals(" "))
            {
                i = rnd.Next(0, 13); // creates a number between 0 and 12
                j = rnd.Next(0, 4);   // creates a number between 0 and 3

                
            }


           
            p.Cards.Add(i+2);
            if (i >= 0 && i <= 7)
            {
                p.Points += (i + 2);
            }
            else if(i>=8 && i <= 11)
            {
                p.Points += 10;
            }
            


            string card = deck[i, j];
            deck[i, j] = " ";
            return card;



        }

        public void ValidateUsersCards()
        {
            for(int i =0; i < Global.player.Cards.Count; i++)
            {
                if (Global.player.Cards[i] == 14)
                {
                    // player has an ace
                    playerButtons.Visible = true;

                }
            }           
        }

        public void ValidateUsersCards(int x)
        {           
                if (x == 14)
                {
                    // player has an ace
                    playerButtons.Visible = true;

                }
        }
       
        //Checks for a bust
        public void Bust(int points, string name)
        {
            
            
            if(name == "Player")
            {
                if (points > 21)
                {
                    //Display Pop Up Notifiing Bust of player and lose of cash
                    whoWon.Visible = true;
                    message.Text = "Sorry, you busted!";
                    earnings.Text = "You lost " + Global.bet+"$";
                    Global.player.Wallet = Global.player.Wallet - Global.bet;
                    wallet.Text = Global.player.Wallet + "";
                    Label1.Text = Global.player.Points.ToString();
                    Label4.Text = Global.dealer.Points.ToString();
                    endMatch(Global.player.Wallet);
                    BtnStay.Visible = false;
                    BtnHit.Visible = false;
                    
                }
            }
            
            else
            {
                if (points > 21)
                {
                    //Display Pop Up Notifiing Bust of Dealer and win of cash  
                     
                    whoWon.Visible = true;
                    message.Text = "Dealer Bust!";
                }
                Label1.Text = Global.player.Points.ToString();
                Label4.Text = Global.dealer.Points.ToString();
            }
           

        }

        public void DetermineWinner()
        {
            double gain;
            //Player Wins
            if ((Global.player.Points <=21 || Global.dealer.Points >21 ))
            {
                //Wins 1.5Times amount of his bet
                if (Global.player.Points == 21 && Global.player.Cards.Count == 2)
                {
                    gain = (1.5 * Global.bet) + Global.bet;
                    Global.player.Wallet += gain;

                    //dislay the placeholder
                    whoWon.Visible = true;
                    wallet.Text = Global.player.Wallet + "";
                    message.Text = Session["player"] + " won";
                    earnings.Text = "You won " + gain + "$";



                    wallet.Text = Global.player.Wallet+"";
                }
                else if(Global.player.Points > Global.dealer.Points || Global.dealer.Points >21) //Gains his bet
                {

                    gain = 2 * Global.bet;
                    Global.player.Wallet += gain;

                    whoWon.Visible = true;
                    wallet.Text = Global.player.Wallet + "";
                    message.Text = Session["player"]+" won";
                    earnings.Text = "You won " + gain + "$";



                    wallet.Text = Global.player.Wallet + "";
                }
            }
            //Tie Game
            if (Global.player.Points == Global.dealer.Points)
            {
                Global.player.Wallet += Global.bet;
                whoWon.Visible = true;
                wallet.Text = Global.player.Wallet + "";
                message.Text = "It's a tie";
                earnings.Text = "0$ Awarded";

            }
            //Dealer Wins
            if (Global.dealer.Points <= 21)
            {
                
                if (Global.dealer.Points == 21 && Global.dealer.Cards.Count == 2)
                {
                                       
                    wallet.Text = Global.player.Wallet + "";
                    whoWon.Visible = true;
                    message.Text = "Dealer won";
                    earnings.Text = "You lost " + Global.bet + "$";
                    endMatch(Global.player.Wallet);


                }
                else if(Global.dealer.Points > Global.player.Points)
                {
                                      
                    wallet.Text = Global.player.Wallet + "";
                    whoWon.Visible = true;
                    message.Text = "Dealer won";
                    earnings.Text = "You lost " + Global.bet + "$";
                    endMatch(Global.player.Wallet);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            wallet.Text = Global.player.Wallet +"";
           
            if (!Page.IsPostBack) {              
                

                

                //Dealer
                Global.dealerFirstCard = generateCard(Global.dealer);
                Global.dealer.Faces.Add(Global.dealerFirstCard);
                card1.ImageUrl = "img/red_back.png"; // setting the path to the image
                card1.Height = 230;
                //card1.Width = 120;
                dealerHand.Controls.Add(card1);


                Global.dealerSecondCard = generateCard(Global.dealer);
                Global.dealer.Faces.Add(Global.dealerSecondCard);
                card2.ImageUrl = "img/" + Global.dealerSecondCard + ".png"; // setting the path to the image
                card2.Height = 230;
                //card2.Width = 120;
                dealerHand.Controls.Add(card2);

                //Player
                for (int i = 0; i < 2; i++)
                {
                    Global.playerCard = generateCard(Global.player);
                    Global.player.Faces.Add(Global.playerCard);
                    players_card.ImageUrl = "img/" + Global.player.Faces[i] + ".png"; // setting the path to the image
                    players_card.Height = 230;
                    ///players_card.Width = 120;
                    playerHand.Controls.Add(players_card);
                    players_card = new Image();
                }

                

                ValidateUsersCards();



            }
            //Testing PostBack
            //player and dealer seems to be reset on each postback            
            else
            {
                
                //Dealer           
                card1.ImageUrl = "img/red_back.png"; // setting the path to the image
                    card1.Height = 230;
                    //card1.Width = 120;
                    dealerHand.Controls.Add(card1);

                    card2.ImageUrl = "img/" + Global.dealer.Faces[1] + ".png"; // setting the path to the image
                    card2.Height = 230;
                    //card2.Width = 120;
                    dealerHand.Controls.Add(card2);

                    //Player
                    for (int i = 0; i < Global.player.Faces.Count; i++)
                    {

                        players_card.ImageUrl = "img/" + Global.player.Faces[i] + ".png"; // setting the path to the image
                        players_card.Height = 230;
                        //players_card.Width = 120;
                        playerHand.Controls.Add(players_card);
                        players_card = new Image();
                    }

                    ValidateUsersCards();               
                
              
            }
        }

        protected void btnStay_Click(object sender, EventArgs e)
        {
            playerButtons.Visible = false;
            card1.ImageUrl = "img/" + Global.dealer.Faces[0] + ".png"; // setting the path to the image            
            BtnStay.Visible = false;
            BtnHit.Visible = false;
            Label1.Text = Global.player.Points.ToString();
            
            //Dealer Deals Additional Cards if below 17 points
            while(Global.dealer.Points < 17)
            {
                card2 = new Image();
                Global.dealerSecondCard = generateCard(Global.dealer);
                Global.dealer.Faces.Add(Global.dealerSecondCard);
                card2.ImageUrl = "img/" + Global.dealerSecondCard + ".png"; // setting the path to the image
                card2.Height = 230;
                //card2.Width = 120;
                dealerHand.Controls.Add(card2);
                
            }
            Label4.Text = Global.dealer.Points.ToString();

            Bust(Global.dealer.Points, "Dealer");
            
            DetermineWinner();
            
            
        }

        protected void btnHit_Click(object sender, EventArgs e)
        {
            
            playerButtons.Visible = false;
            Global.playerCard = generateCard(Global.player);
            Global.player.Faces.Add(Global.playerCard);
            players_card.ImageUrl = "img/" + Global.player.Faces[Global.player.Faces.Count-1] + ".png"; // setting the path to the image
            players_card.Height = 230;
            //players_card.Width = 120;
            playerHand.Controls.Add(players_card);
            players_card = new Image();

            ValidateUsersCards(Global.player.Cards.Last());
            Bust(Global.player.Points, "Player");


        }

        protected void btOnePoint_Click(object sender, EventArgs e)
        {
            Global.player.Points +=1;
            playerButtons.Visible = false;
        }

        protected void btElevenPoints_Click(object sender, EventArgs e)
        {
            Global.player.Points +=11;
            playerButtons.Visible = false;
        }

        
        [WebMethod]
        public static  string update(string userdata)
        {
            int bet = int.Parse(userdata);
            Global.bet = bet;
            //Global.player.Wallet -= bet;
            

            return "Posted"+userdata;
        }

        protected void continue_Click(object sender, EventArgs e)
        {
            Global.player.Cards = new List<int>();
            Global.dealer.Cards = new List<int>();
            Global.player.Faces = new List<string>();
            Global.dealer.Faces = new List<string>();
            Global.player.Points = 0;
            Global.dealer.Points = 0;
            Response.Redirect("Main.aspx");
        }
    }
}