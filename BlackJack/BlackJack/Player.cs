﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BlackJack
{
    public class Player
    {
        List<int> cards = new List<int>();
        int points;
        double cash;
        List<string> faces = new List<string>();


        public int Points { get => points; set => points = value; }
        public List<int> Cards { get => cards; set => cards = value; }
        public double Wallet { get => cash; set => cash = value; }
        public List<string>Faces { get =>faces; set => faces = value; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach(int i in Cards)
            {
                str.Append(i + ",");
            }
            foreach (string s in Faces)
            {
                str.Append(s + ",");
            }
            str.Append(Wallet + ",");
            str.Append(Points);
            return str.ToString();


        }
    }
}