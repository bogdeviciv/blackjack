﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace BlackJack
{
    public class Global : System.Web.HttpApplication
    {
        
        public static Player player = new Player();
        
        public static Player dealer = new Player();

        public static string dealerFirstCard;
        public static string dealerSecondCard = "";
        public static string playerCard = "";
        public static double bank = 0;
        public static double bet;


        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}