﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="BlackJack.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet"/>
    
    <link href="/Content/styles.css" rel="stylesheet" />
    <title>BlackJack</title>
</head>
<body>
    <script>
      
        var bet = 0;
        function isPostBack() {
  return "<%= Page.IsPostBack %>"==="False"? false : true;
        }
     
        $(window).load(function () {
            if (!isPostBack()) {
                $('#myModal').modal('show');                
            }
            $("#add").click(function () {
                if (bet < 100) {
                    bet += 10;
                }                
                $("#bet").val(bet);
                });
            
             $("#substract").click(function () {
                    if (bet <= 10) return;
                bet -= 10;
                 $("#bet").val(bet);
               
            });
            //place a bet ajax call
            $("#postBet").click(function () {
                var bet=$("#bet").val();
                var DTO = { 'userdata': bet };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Main.aspx/update",
                    data: JSON.stringify(DTO),
                    datatype: "json",
                    success: function (result) {
                        //do something
                        //alert("SUCCESS = " + result.d);
                        console.log(result);
                    },
                    error: function (xmlhttprequest, textstatus, errorthrown) {
                        alert(" conection to the server failed ");
                        console.log("error: " + errorthrown);
                    }
                });//end of $.ajax()
                $('#myModal').modal('hide');               
            });
  
});</script>


    <div class="modal" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Betting</h5>

      </div>
      <div class="modal-body">
        <p>Make your bet! </p>
          <button type="button" id="add" ><img src="/img/add.png" alt="add"/></button>
          <input type="number" id="bet" name="bet" value="0" readonly="readonly"  /> 
          <button type="button" id="substract" ><img src="/img/substract.png" alt="add"/></button><br>
          
      </div>
      <div class="modal-footer">
        <button id="postBet" type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <form id="MycontainerMain" runat="server">
    

    
    <div id="dealer" style="height: 30%">
    <h3>Dealer</h3>
        <asp:Label ID="dealerPoints" runat="server"><%%></asp:Label>
    <div class="Myimages">       
    <asp:PlaceHolder ID="dealerHand" runat="server"></asp:PlaceHolder>
        
        </div>
    </div>
        <div id="player" style="height: 70%">

            <div class="Myimages">
                <asp:PlaceHolder ID="playerHand" runat="server"></asp:PlaceHolder>
            </div>

            <div class="buttons">
                <asp:LinkButton class="btn btn-success" ID="BtnHit" OnClick="btnHit_Click" runat="server">Hit</asp:LinkButton>
                <asp:LinkButton class="btn btn-success" ID="BtnStay" OnClick="btnStay_Click" runat="server">Stay</asp:LinkButton>
                <asp:PlaceHolder ID="playerButtons" Visible="false" runat="server">
                    <h6>You have an Ace. How would you like to play it ? </h6>

                    <asp:LinkButton class="btn btn-success" ID="LinkButton1" runat="server" OnClick="btOnePoint_Click">1 point</asp:LinkButton>
                    <asp:LinkButton class="btn btn-success" ID="btElevenPoints" runat="server" Text="11 points" OnClick="btElevenPoints_Click">11 point</asp:LinkButton>

                </asp:PlaceHolder>
            </div>
            <div id="infos">
                <asp:Label ID="Label2" runat="server" Text="Player"></asp:Label>
                <asp:Label ID="Label1" runat="server" Text="0"></asp:Label><br/>
                <asp:Label ID="Label3" runat="server" Text="Dealer"></asp:Label>
                <asp:Label ID="Label4" runat="server" Text="0"></asp:Label><br/>
                <h5 style="display: inline;">Your wallet:
                    <asp:Label ID="wallet" runat="server" Text="0"></asp:Label></h5><br/>
                <asp:PlaceHolder ID="whoWon" Visible="false" runat="server">                   
                    <asp:Label ID="message" runat="server" Text=""></asp:Label><br/>
                    <asp:Label ID="earnings" runat="server" Text=""></asp:Label><br/>
                    <asp:Label ID="gameOver" runat="server" Text=""></asp:Label><br/>
                    <asp:LinkButton class="btn btn-success" ID="continue" runat="server" OnClick="continue_Click" >Continue</asp:LinkButton>
                </asp:PlaceHolder>
            </div>
            <h3><%:Session["player"]%></h3>
        </div>
            
    
    </form>
</body>
</html>
